/*
 Navicat Premium Data Transfer

 Source Server         : localhost_5432
 Source Server Type    : PostgreSQL
 Source Server Version : 140002
 Source Host           : localhost:5432
 Source Catalog        : redditClone
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140002
 File Encoding         : 65001

 Date: 15/05/2022 19:04:35
*/


-- ----------------------------
-- Sequence structure for comment_comment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."comment_comment_id_seq";
CREATE SEQUENCE "public"."comment_comment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_post_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_post_id_seq";
CREATE SEQUENCE "public"."post_post_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for subreddit_subreddit_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."subreddit_subreddit_id_seq";
CREATE SEQUENCE "public"."subreddit_subreddit_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS "public"."comment";
CREATE TABLE "public"."comment" (
  "comment_id" int4 NOT NULL DEFAULT nextval('comment_comment_id_seq'::regclass),
  "comment_text" text COLLATE "pg_catalog"."default",
  "last_update" date NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "vote_count" int4,
  "post_id" int4
)
;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO "public"."comment" VALUES (48, 'docker
', '2022-05-15', NULL, NULL);

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS "public"."post";
CREATE TABLE "public"."post" (
  "post_id" int4 NOT NULL DEFAULT nextval('post_post_id_seq'::regclass),
  "post_title" varchar(255) COLLATE "pg_catalog"."default",
  "post_description" text COLLATE "pg_catalog"."default",
  "post_url" varchar(255) COLLATE "pg_catalog"."default",
  "last_update" date,
  "subreddit_id" int4,
  "post_create_date" date NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "vote_count" int4
)
;

-- ----------------------------
-- Records of post
-- ----------------------------

-- ----------------------------
-- Table structure for subreddit
-- ----------------------------
DROP TABLE IF EXISTS "public"."subreddit";
CREATE TABLE "public"."subreddit" (
  "subreddit_id" int4 NOT NULL DEFAULT nextval('subreddit_subreddit_id_seq'::regclass),
  "subreddit_title" varchar(255) COLLATE "pg_catalog"."default",
  "last_update" date,
  "subreddit_create_date" date NOT NULL DEFAULT CURRENT_TIMESTAMP
)
;

-- ----------------------------
-- Records of subreddit
-- ----------------------------
INSERT INTO "public"."subreddit" VALUES (33, 'music', NULL, '2022-05-14');
INSERT INTO "public"."subreddit" VALUES (35, 'nida', NULL, '2022-05-15');
INSERT INTO "public"."subreddit" VALUES (36, 'sport', NULL, '2022-05-15');
INSERT INTO "public"."subreddit" VALUES (39, 'nida', NULL, '2022-05-15');
INSERT INTO "public"."subreddit" VALUES (40, 'sun', NULL, '2022-05-15');
INSERT INTO "public"."subreddit" VALUES (32, 'sport hi', '2022-05-15', '2022-05-14');

-- ----------------------------
-- Function structure for trigger_set_timestamp
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."trigger_set_timestamp"();
CREATE OR REPLACE FUNCTION "public"."trigger_set_timestamp"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
  NEW.last_update = NOW();
  RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."comment_comment_id_seq"
OWNED BY "public"."comment"."comment_id";
SELECT setval('"public"."comment_comment_id_seq"', 56, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."post_post_id_seq"
OWNED BY "public"."post"."post_id";
SELECT setval('"public"."post_post_id_seq"', 26, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."subreddit_subreddit_id_seq"
OWNED BY "public"."subreddit"."subreddit_id";
SELECT setval('"public"."subreddit_subreddit_id_seq"', 41, true);

-- ----------------------------
-- Triggers structure for table comment
-- ----------------------------
CREATE TRIGGER "set_timestamp" BEFORE UPDATE ON "public"."comment"
FOR EACH ROW
EXECUTE PROCEDURE "public"."trigger_set_timestamp"();

-- ----------------------------
-- Primary Key structure for table comment
-- ----------------------------
ALTER TABLE "public"."comment" ADD CONSTRAINT "comment_pkey" PRIMARY KEY ("comment_id");

-- ----------------------------
-- Triggers structure for table post
-- ----------------------------
CREATE TRIGGER "set_timestamp" BEFORE UPDATE ON "public"."post"
FOR EACH ROW
EXECUTE PROCEDURE "public"."trigger_set_timestamp"();

-- ----------------------------
-- Primary Key structure for table post
-- ----------------------------
ALTER TABLE "public"."post" ADD CONSTRAINT "post_pkey" PRIMARY KEY ("post_id");

-- ----------------------------
-- Triggers structure for table subreddit
-- ----------------------------
CREATE TRIGGER "set_timestamp" BEFORE UPDATE ON "public"."subreddit"
FOR EACH ROW
EXECUTE PROCEDURE "public"."trigger_set_timestamp"();

-- ----------------------------
-- Primary Key structure for table subreddit
-- ----------------------------
ALTER TABLE "public"."subreddit" ADD CONSTRAINT "subreddit_pkey" PRIMARY KEY ("subreddit_id");

-- ----------------------------
-- Foreign Keys structure for table comment
-- ----------------------------
ALTER TABLE "public"."comment" ADD CONSTRAINT "comment_post__fk" FOREIGN KEY ("post_id") REFERENCES "public"."post" ("post_id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post
-- ----------------------------
ALTER TABLE "public"."post" ADD CONSTRAINT "sub_po_id" FOREIGN KEY ("subreddit_id") REFERENCES "public"."subreddit" ("subreddit_id") ON DELETE CASCADE ON UPDATE CASCADE;
