package com.example.g3_sr_redditclone.repository;

import com.example.g3_sr_redditclone.model.Comment;
import com.example.g3_sr_redditclone.model.Post;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository {

    @Select("select * from comment")
    List<Comment> getAllComment();

    @Select("select * from comment where comment_id=#{id}")
    Comment findCommentById(int id);

    @Select("select * from comment where post_id=#{id}")
    List<Comment> findCommentByPostId(int id);


    @Insert("insert into comment (comment_text, post_id) " +
            "values (#{comment.comment_text}, #{comment.post_id})")
    public Boolean insertComment(@Param("comment") Comment comment);

    @Select("select count(*) from comment")
    public void countComment();

    @Update("update comment set vote_count=#{comment.vote_count} where comment_id=#{id}")
    public Boolean updateVote(@Param("comment") Comment comment, int id);


}
