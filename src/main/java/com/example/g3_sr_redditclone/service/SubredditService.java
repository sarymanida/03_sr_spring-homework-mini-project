package com.example.g3_sr_redditclone.service;

import com.example.g3_sr_redditclone.model.Comment;
import com.example.g3_sr_redditclone.model.Subreddit;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SubredditService {

    List<Subreddit> getAllSubreddit();

    Subreddit findSubredditById(int id);

    Boolean updateSubreddit(Subreddit subreddit,int id);

    Boolean deleteSubreddit(int id);

    Boolean insertSubreddit(Subreddit subreddit);

    String getSubTitle(int id);

}
